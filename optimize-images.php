<?php
/* 
Plugin Name: 	Optimize Images and Convert to WebP
Plugin URI: 	https://acmatvirus.top
Description: 	This plugin optimizes uploaded images and converts them to WebP format by AcmaTvirus.
Tags: 			Acmatvirus, Plugin Optimize Images and Convert to WebP
Author: 		Acmatvirus
Author URI: 	https://acmatvirus.top
Version: 		1.0
License: 		GPL2
Text Domain:    Acmatvirus
*/

include_once "config.php";
include_once "application/helper/data_helper.php";
include_once "system/core.php";
include_once "system/admin.php";
foreach (glob(plugin_dir_path(__FILE__) . 'application/controller/*.php') as $file) {
    include_once $file;
}
