<?php
// Load WordPress
require_once('../../../wp-load.php');

$url = $_POST['url'];
function optimize_and_convert_to_webp_clone($uploaded_file)
{
    $image_info = @getimagesize($uploaded_file);
    if ($image_info !== false && in_array($image_info[2], array(IMAGETYPE_JPEG, IMAGETYPE_PNG, IMAGETYPE_WEBP))) {
        // Tối ưu hóa ảnh sử dụng thư viện bên ngoài (ví dụ: Imagick)
        optimize_image_clone($uploaded_file);
    };
    return filesize(str_replace(["\\", "\n"], ["/", "/n"], $uploaded_file));
}

function optimize_image_clone($image_path)
{
    $imagick = new Imagick($image_path);
    $imagick->setImageCompressionQuality(80);
    $imagick->writeImage($image_path);
    $imagick->clear();
    $imagick->destroy();
}

echo optimize_and_convert_to_webp_clone($url);
