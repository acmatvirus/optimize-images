<?php if (!defined('WPINC')) die;

define('OIM_BASE_URL', get_option('siteurl'));
define('OIM_URL', plugin_dir_url(__FILE__));
define('OIM_DERICTORY', plugin_dir_path(__FILE__));

define('OIM_TITLE', 'Optimize Images');
define('OIM_SLUG', str_replace('/config.php', '', plugin_basename(__FILE__)));
define('OIM_VERSION', json_decode(file_get_contents(OIM_DERICTORY . '/package.json'))->version);
define('OIM_ASSET_VERSION', '1.0.0');