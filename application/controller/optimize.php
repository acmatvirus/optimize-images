<?php
add_filter('wp_handle_upload', 'optimize_and_convert_to_webp');

function optimize_and_convert_to_webp($upload)
{
    $uploaded_file = $upload['file'];
    if (exif_imagetype($uploaded_file) === IMAGETYPE_JPEG || exif_imagetype($uploaded_file) === IMAGETYPE_PNG) {
        $webp_file = preg_replace('/\\.[^.\\s]{3,4}$/', '', $uploaded_file) . '.webp';
        if (imagewebp(imagecreatefromstring(file_get_contents($uploaded_file)), $webp_file, 80)) {
            unlink($uploaded_file);
        }
        $upload['file'] = $webp_file;
    }

    return $upload;
}
