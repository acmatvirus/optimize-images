<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" />
<!--  -->
<header style="height: 90px; padding: 15px 0; text-align: center;">
  <img src="<?= OIM_URL . 'public/img/banner.png' ?>" height="90px" alt="">
</header>
<h2>Optimize Images Settings</h2>
<!--  -->
<ul id="tabs">
  <li data-tab="#tab-01" class="current">Setting</li>
  <li data-tab="#tab-02">Statistic</li>
</ul>
<!--  -->
<div class="tab-content current" id="tab-01">
  <div class="wrap">
    <form action="" method="post">
      <p>API KEY: <?= !empty($data['data']->key) ? $data['data']->key : 'No key'; ?></p>
      <?php if (empty($data['data']->key)) : ?>
        <input type="hidden" name="domain" value="<?= OIM_BASE_URL; ?>">
        <button name="type" value="generate" class="button-primary">Create an application key</button>
      <?php endif; ?>
      <?php if (!empty($data['data']->key)) : ?>
        <input type="hidden" name="key" value="<?= $data['data']->key; ?>">
        <button name="type" value="optimize" class="button-primary">Optimize Images</button>
      <?php endif; ?>
    </form>
    <?php if (!empty($data['data']->key)) : ?>
      <br>
      <form action="" method="post">
        <input type="hidden" name="key" value="<?= $data['data']->key; ?>">
        <button name="type" value="request" class="button-primary">Request Optimize Images</button>
      </form>
    <?php endif; ?>
  </div>
</div>
<div class="tab-content" id="tab-02">

</div>
<script src="<?= OIM_URL ?>public/js/admin.js"></script>