<?php
$data = json_decode(get_option('setting_' . OIM_SLUG), true);
$data_api = json_decode(get_option('setting_' . OIM_SLUG));
$message = 'a';
if (empty($data)) $data = [];
if (isset($_POST['type']) && $_POST['type'] == 'generate') {
    $message = '<div class="updated"><p>Cấu hình đã được lưu.</p></div>';
};
if (isset($_POST['save_settings'])) {
    $tab = $_POST['tab'];
    unset($_POST['tab']);
    unset($_POST['save_settings']);
    $save = json_encode(array_merge($data, $_POST));
    update_option('setting_lottery', $save);
    $message = '<div class="updated"><p>Cấu hình đã được lưu.</p></div>';
    $data = json_decode(get_option('setting_lottery'), true);
};
