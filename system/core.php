<?php

if (!function_exists('oim_leftbar')) {
  function oim_leftbar()
  {
    add_options_page(OIM_TITLE, OIM_TITLE, 'manage_options', OIM_SLUG, function () {
      if (extension_loaded('imagick')) {
        $view = new ViewOIM();
        $save = [];
        if (isset($_POST['type']) && $_POST['type'] == 'generate') {
          $key = $view->curl('https://socketapi.site/api/plugin/optimizeimage/generate', ['domain' => OIM_BASE_URL]);
          $save['key'] = $key;
          $save = json_encode(array_merge($save, $_POST));
          update_option('setting_' . OIM_SLUG, $save);
          echo '<div class="updated"><p>The application key has been created.</p></div>';
        };
        if (isset($_POST['type']) && $_POST['type'] == 'optimize') {
          $upload_dir = wp_upload_dir();
          $uploads_folder = $upload_dir['basedir'];
          $image_files = get_all_images_in_directory($uploads_folder);
          $data_image = [];
          foreach ($image_files as $key => $value) {
            $data_image[] = [
              'url' => $value,
              'filename' => pathinfo($value)['basename'],
              'filectime' => filectime($value),
              'filesize' => filesize($value)
            ];
          };
          $message = $view->curl('https://socketapi.site/api/plugin/optimizeimage/save', ['key' => $_POST['key'], 'data' => json_encode($data_image)]);
          echo '<div class="updated"><p>' . $message . '</p></div>';
        };
        if (isset($_POST['type']) && $_POST['type'] == 'request') {
          $message = $view->curl('https://socketapi.site/api/plugin/optimizeimage/request', ['key' => $_POST['key']]);
          echo '<div class="updated"><p>' . $message . '</p></div>';
        };
        $config = json_decode(file_get_contents("https://gitlab.com/acmatvirus/optimize-images/-/raw/main/package.json"));
        if (OIM_VERSION !== $config->version) {
          $zip_url = 'https://gitlab.com/acmatvirus/optimize-images/-/archive/main/optimize-images-main.zip';
          $zip_content = file_get_contents($zip_url);
          if ($zip_content === false) {
            echo '<div class="updated"><p>Error: cannot download zip file</p></div>';
          }
          $tmp_zip_path = '../wp-content/plugins/optimize-images-main.zip';
          file_put_contents($tmp_zip_path, $zip_content);
          $zip = new ZipArchive;
          if ($zip->open($tmp_zip_path) === true) {
            $zip->extractTo("../wp-content/plugins/");
            $zip->close();
            shell_exec("service php-fpm reload");
            echo '<div class="updated"><p>Update new version ' . $config->version . ' success</p></div>';
          } else {
            echo '<div class="updated"><p>Error: cannot open zip file</p></div>';
          }
          unlink($tmp_zip_path);
        }
        $data = json_decode(get_option('setting_' . OIM_SLUG));
        echo $view->render('admin/index.php', [
          'data' => $data
        ]);
      } else {
        echo 'PHP does not support Imagick.';
      }
    });
  }

  function get_all_images_in_directory($directory)
  {
    $image_files = array();

    // Lấy danh sách tất cả các tệp ảnh trong thư mục
    $files = glob($directory . '/*.{jpg,jpeg,png,webp}', GLOB_BRACE);
    $image_files = array_merge($image_files, $files);

    // Lấy danh sách tất cả các thư mục con
    $subdirectories = glob($directory . '/*', GLOB_ONLYDIR);

    // Duyệt qua từng thư mục con và lấy danh sách ảnh
    foreach ($subdirectories as $subdirectory) {
      $image_files = array_merge($image_files, get_all_images_in_directory($subdirectory));
    }

    return $image_files;
  }
}
add_action('admin_menu', 'oim_leftbar');

// Enqueue styles for admin
add_action('admin_enqueue_scripts', function () {
  wp_enqueue_style(OIM_SLUG . 'admin-style', OIM_URL . 'public/css/admin.css', [], OIM_ASSET_VERSION);
});

// Enqueue scripts for admin
add_action('admin_enqueue_scripts', function () {
  wp_enqueue_script(OIM_SLUG . '-admin-script', OIM_URL . 'public/js/admin.js', array('jquery'), OIM_ASSET_VERSION, true);
});

// Enqueue styles for frontend
add_action('wp_enqueue_scripts', function () {
  wp_enqueue_style(OIM_SLUG . '-style', OIM_URL . 'public/css/style.css', [], OIM_ASSET_VERSION);
});

// Enqueue scripts for frontend
add_action('wp_enqueue_scripts', function () {
  wp_enqueue_script('contact-button-script', OIM_URL . 'public/js/main.js', array('jquery'), OIM_ASSET_VERSION, true);
});

if (!class_exists('ViewOIM')) {
  class ViewOIM
  {
    protected $data;
    function render($filename, $data)
    {
      ob_start();
      $this->data = $data;
      include_once OIM_DERICTORY . "/application/helper/data_helper.php";
      require OIM_DERICTORY . '/application/view/' . $filename;
      $str = ob_get_contents();
      ob_end_clean();
      return $str;
    }

    function curl($url, $data, $method = 'GET')
    {
      // Khởi tạo một tài liệu cURL
      $ch = curl_init();
      // Thiết lập các tùy chọn của yêu cầu cURL
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $response = curl_exec($ch);
      if (curl_errno($ch)) {
        echo 'CURL Error: ' . curl_error($ch);
      }
      curl_close($ch);
      return $response;
    }
  }
}
