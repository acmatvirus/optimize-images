jQuery(document).ready(function ($) {
  /*Tabbing*/
  jQuery('#tabs li').on('click', function ($) {
    var el = jQuery(this);
    var tab_name = el.data('tab');
    el.addClass('current').siblings().removeClass('current');
    jQuery(tab_name).addClass('current').siblings().removeClass('current');
  });
})